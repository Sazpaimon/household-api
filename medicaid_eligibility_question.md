# Medicaid Eligibility
*This exercise is meant to simulate the experience of developing production code, so please follow whatever development process you would normally use for production code, to the extent that you can in an interview setting. For example, if you would normally write your tests first, please do so here.*

*Also, this question is purposefully ambiguous because we believe there's more to a question than just the answer. Remember that your interviewer is here to help you show off your skills, so feel free to ask her or him anything that will help you.*


## Introduction
Our recommendations for ACA (Obamacare) plans are done on the household level. However, we want to exclude the subset of the household that is eligible for Medicaid from consideration for ACA plan recommendations, because Medicaid is a better option for them. We need our system to determine which subset of the household is eligible for Medicaid. The remainder of the household will be handed off to the recommendation engine to produce ACA recommendations. (The recommendation engine is not part of the scope of the problem -- this is just to provide context.)


## Problem
Please write some code (e.g. a function, a method on a class) in the language of your choice that accepts a household object (which you should define) and tells you which members are and are not eligible for Medicaid, given the below specifications.


## Specifications

### Eligibility rules
* Eligibility is based on [Federal Poverty Limit](http://aspe.hhs.gov/poverty/15poverty.cfm)
* Percentages determined by `(total household income) / FPL`
* Children 2-18 eligible @ 275% FPL
* Infants 283% FPL
* Pregnant women 278% FPL
* Everyone 200% FPL

### Available person information
* Age
* Gender
* Pregnancy status

### Available household information
* Income
