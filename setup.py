import sys

from setuptools import setup, find_packages

needs_pytest = {'pytest', 'test', 'ptr'}.intersection(sys.argv)
pytest_runner = ['pytest-runner'] if needs_pytest else []

pylint_runner = ['setuptools-lint'] if 'lint' in sys.argv else []

tests_require = [
    'pylint',
    'pytest',
    'pytest-cov',
    'pytest-pylint',
    'pytest-flask',
    'pytest-mock',
    'mockredispy'
]

setup(
    name='Household API',
    description='Household Resource REST API',
    version='1.0.0',
    author='Ian Carpenter',
    author_email='icarpenter@cultnet.net',
    url='https://gitlab.com/sazpaimon/household-api',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    install_requires=[
        'flask',
        'schematics',
        'redis',
        'coverage',
        'shortuuid'
    ],
    tests_require=tests_require,
    extras_require={'test': tests_require},
    setup_requires=[] + pytest_runner + pylint_runner
)
