GREEN='\033[0;32m'
NC='\033[0m'
export DOCKER_IP="$(docker-machine ip default)"
echo "Dev server will be accessible on ${GREEN}http://${DOCKER_IP}:5000/${NC}"

docker-compose build
docker-compose up --force-recreate
