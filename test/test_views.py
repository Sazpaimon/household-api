import json

import pytest
from flask import Response
from flask.testing import FlaskClient
from mockredis.client import MockRedis
from pytest_mock import MockFixture
from schematics.exceptions import BaseError

from household_api.household import views
from household_api.household.models import Household, User
from household_api.household.views import POVERTY_INCOME_BASE, \
    POVERTY_INCOME_PER_PERSON


@pytest.fixture(scope='session')
def app():
    from household_api.app import app

    app.debug = True

    return app


@pytest.fixture()
def redis():
    return MockRedis(strict=True)


def test_create_household_saves_incoming_data(
        client: FlaskClient, redis: MockRedis, mocker: MockFixture):
    mocker.patch('household_api.household.views.get_redis_connection',
                 return_value=redis)
    mocker.patch('shortuuid.uuid', return_value='foo')
    mock_household = {
        'income': 1,
        'members': [
            {'age': 1, 'gender': 'male'},
        ],
    }
    res: Response = client.post(
        '/household',
        content_type='application/json',
        data=json.dumps(mock_household)
    )
    assert res.status_code == 200
    assert json.loads(res.data) == {'household_id': 'foo'}
    assert json.loads(redis.get('foo')) == mock_household


def test_create_household_returns_400_when_post_validation_fails(
        client: FlaskClient, mocker: MockFixture):
    mock_validation_error = 'Could not foo bar'
    mocker.patch.object(Household, 'validate',
                        side_effect=BaseError([mock_validation_error]))
    invalid_household = Household({
        'income': 12345,
        'members': [],
    })
    res: Response = client.post(
        '/household',
        content_type='application/json',
        data=json.dumps(invalid_household.to_primitive())
    )

    assert res.status_code == 400
    assert json.loads(res.data)['message'] == [mock_validation_error]


def test_get_household_returns_stored_data(
        client: FlaskClient, redis: MockRedis, mocker: MockFixture):
    mocker.patch('household_api.household.views.get_redis_connection',
                 return_value=redis)
    mock_household = {
        'income': 50000,
        'members': [
            {'age': 45, 'gender': 'female'},
            {'age': 40, 'gender': 'male'},
        ],
    }
    redis.set('foo', json.dumps(mock_household))
    res: Response = client.get('/household/foo')
    assert res.status_code == 200
    assert json.loads(res.data) == mock_household


def test_get_household_returns_404_when_household_not_found(
        client: FlaskClient, redis: MockRedis, mocker: MockFixture):
    mocker.patch('household_api.household.views.get_redis_connection',
                 return_value=redis)
    redis.set('foo', json.dumps({
        'income': 1,
        'members': [
            {'age': 1, 'gender': 'male'},
        ],
    }))
    res: Response = client.get('/household/bar')
    assert res.status_code == 404
    assert json.loads(res.data)['message'] == 'Household not found'


def test_get_household_poverty_level_calculates_poverty_level_percentage(
        client: FlaskClient, redis: MockRedis, mocker: MockFixture):
    mocker.patch('household_api.household.views.get_redis_connection',
                 return_value=redis)
    # Set the income of the mock household to be exactly 100% of the
    # poverty threshold
    poverty_threshold = POVERTY_INCOME_BASE + POVERTY_INCOME_PER_PERSON
    redis.set('foo', json.dumps({
        'income': poverty_threshold,
        'members': [
            {'age': 1, 'gender': 'male'},
        ],
    }))
    res: Response = client.get('/household/poverty_level/foo')
    assert res.status_code == 200
    assert json.loads(res.data) == {
        "income": poverty_threshold,
        "poverty_threshold": poverty_threshold,
        "percentage_poverty_level": 1.0
    }


def test_is_medicaid_eligible():
    household_member = User({'age': 40, 'gender': 'male'})
    household_fpl_percent = 1
    assert views.is_medicaid_eligible(household_member,
                                      household_fpl_percent) is True
    household_member_child = User({'age': 2, 'gender': 'male'})
    household_fpl_percent = 2.76
    assert views.is_medicaid_eligible(household_member_child,
                                      household_fpl_percent) is False
    household_member_pregnant = User(
        {'age': 25, 'gender': 'female', 'pregnant': True}
    )
    household_fpl_percent = 2.79
    assert views.is_medicaid_eligible(household_member_pregnant,
                                      household_fpl_percent) is False

    household_member_male = User(
        {'age': 25, 'gender': 'male'}
    )
    household_fpl_percent = 2.01
    assert views.is_medicaid_eligible(household_member_male,
                                      household_fpl_percent) is False

    household_member_pregnant_teen = User(
        {'age': 16, 'gender': 'female', 'pregnant': True}
    )
    household_fpl_percent = 2.76
    assert views.is_medicaid_eligible(household_member_pregnant_teen,
                                      household_fpl_percent) is True
