[![build status](https://gitlab.com/Sazpaimon/household-api/badges/master/build.svg)](https://gitlab.com/Sazpaimon/household-api/commits/master)
[![coverage report](https://gitlab.com/Sazpaimon/household-api/badges/master/coverage.svg)](https://gitlab.com/Sazpaimon/household-api/commits/master)

We tried to design this problem to take a couple of hours at most, _but_ it's possible that we wildly miscalculated, especially if you're not familiar with the toolset. We've done our best to set you up for success, but we have likely missed some things. Submit whatever progress you make in a reasonable amount of time, and please contact us if you get really stuck. Happy coding!

# Household Resource REST API

## Introduction
The inputs required to generate a Picwell health insurance recommendation include household information and information about the plans for which the household is eligible. We would like to store the households, rather than accept the information newly for each recommendation, so that we can track how many people are using our service, and also to see how a household's needs and choices change over the years.

## Problem Definition
Please design and implement a [REST API](http://code.tutsplus.com/tutorials/a-beginners-guide-to-http-and-rest--net-16340) household resource that allows an API consumer to create a household and read it back later. Also, please document your API, including routing and example payloads and responses, so that we can run the application.

A household has income and members (people). Household members have age and gender information. We suggest using JSON for serialization.

Based on a household's income and size, a percentage of [Federal Poverty Level (FPL)](https://aspe.hhs.gov/poverty-guidelines) can be calculated.
The percentage is simply `Household.income` divided by the applicable FPL for that household size. One can use the levels associated with
residence in the contiguous states for this exercise.

### Objectives
We recommend that you tackle these one at a time. 1 or 2 completed objectives is better than 3 partially-finished ones. :-)

* Allow API users to create a new household resource
* Allow API users to retrieve a household by unique id
* Return an HTTP 400 Bad Request response if API users try to create a household with an invalid format
* Allow API users to retrieve a household's "percentage of Federal Poverty Level" when supplied a household unique id

For each of the objectives listed above, please provide [unit tests](http://googletesting.blogspot.com/2015/04/just-say-no-to-more-end-to-end-tests.html) to demonstrate correctness of your implementation.

## Getting Started
We have provided a skeleton Flask application to help you get started. [Flask](http://flask.pocoo.org/) is a Python-based web microframework that has very little scaffolding. Hopefully the skeleton app will make it easy to pick up, even if you've never used Flask (or Python) before.

### Running the Application
The application is set up to run in a docker container, so that you can run it without needing to install a bunch of dependencies into your local environment. It also makes it much easier for us to reproduce your results.

If you haven't already, install the [Docker Toolbox](https://www.docker.com/toolbox). When that's finished, run the Docker Quickstart Terminal tool, which will set up a Docker Machine called `default`. It will dump you out into an ordinary shell, which you can then use to run Docker commands. If you would prefer to use your own shell setup, you can run the following command to configure a new shell to talk to your Docker Machine:

```
# in bash or zsh
eval "$(docker-machine env default)"

# in powershell
docker-machine.exe env --shell=powershell default | Invoke-Expression
```

Finally, cd into your project root (the directory that holds this README) and run `./up.sh` to start the application. It may be slow to start the first time because it has to download the base image, but it will be faster on subsequent startups. At the top, it will print something like `Dev server will be accessible on http://<ip-address>:5000/`, with the url in green. Use this url to make requests to the application. To make a request against the test route, try:

```
curl http://<ip-address>:5000/sample-household/
```

To kill the server, just hit ctrl-C. `./up.sh` will start the server again.

Note: if you are familiar with Python tools and would prefer to develop with the application running locally, that's completely fine! Just please make sure that it runs properly in Docker before you submit, so that we can run your app too. And, as always, if you have problems getting the app to run in Docker, please contact us for help.

### Flask Stuff
The application is organized into a single file, for simplicity in getting started. Please feel free to reorganize it however you wish. Be aware, though, that registering Flask routes outside of the main application file `household_api.py` is [a little tricky](http://flask.pocoo.org/docs/0.10/blueprints/).

The imported `jsonify` utility takes a dict or keyword arguments and returns a Flask Response object with an `application/json` mimetype. It is convenient for sending JSON responses.

The Flask `request` object is a thread-global object that contains all of the information for the current request. Access querystring arguments with `request.args`. Retrieve the JSON payload as Python native from `request.json`.


### Domain Models
We have provided `Household` and `User` domain models using the [Schematics](https://schematics.readthedocs.org/en/latest/) library. You should be able to use these without modification for building your REST API.

As demonstrated in the example route `get_sample_household`, a Schematics object is instantiated with a Python dict and can be serialized back to a dict with the `to_primitive` method. Schematics models also provide a `validate` method, which raises a nicely-formatted exception if the model object doesn't fit the specification.

```
In [1]: from household_api import Household

In [2]: Household({"members": [], "income": 0}).validate()
---------------------------------------------------------------------------
ModelValidationError: {'members': [u'Please provide at least 1 item.']}
```

Hint: The `validate` method on Schematics objects can come in handy when writing unit tests.

### Redis
We have also set you up to use Redis, a straightforward key-value store, for storing your household data. The Redis server and Redis python client are installed for you in your docker container, and we've written a small helper for accessing redis.

You should be able to achieve your storage needs using the most basic Redis commands, though of course you may get as fancy as you wish. Hint: one way to do this is to store the household object as a JSON string.

```
In [1]: from household_api import get_redis_connection

In [2]: r = get_redis_connection()

In [3]: r.set('key', '{"household": "object"}')
Out[3]: True

In [4]: r.get('key')
Out[4]: '{"household": "object"}'
```

The documentation for all of the Redis commands is [here](http://redis.io/commands). We are using the python Redis client, so all of the commands are methods against the Redis connection object, which you can retrieve using the `get_redis_connection` helper function that we've provided. For example, the `.set('key', 'value')` method demonstrated above is documented as the `SET` command in the Redis docs.

## Helpful Resources
_Note: We do not expect you to use all or even any of the tools below: feel free to solve the problem in your own way and use whatever tools you prefer. These resources are here to help you find Python-specific tools that you might need, so that you can show off your software development skills even if you aren't super familiar with the Python ecosystem._

* It might be useful to know how Python [exception-handling](https://docs.python.org/2/tutorial/errors.html#handling-exceptions) works.

* Python has a built-in [json library](https://docs.python.org/2/library/json.html) that can encode dicts and lists as JSON strings and decode JSON strings back to dicts or lists. The usage is simple:

```
In [1]: import json

In [2]: json.dumps({'key': 'value'})
Out[2]: '{"key": "value"}'

In [3]: json.loads('{"key": "value"}')
Out[3]: {u'key': u'value'}
```

It also raises exceptions when supplied invalid JSON strings:
```
In [4]: json.loads('("this","isnt","json")')
---------------------------------------------------------------------------
ValueError                                Traceback (most recent call last)
<ipython-input-3-4613fa983c7b> in <module>()
----> 1 json.loads('("this","isnt","json")')
```

* For generating unique ids for newly-generated resources, check out the [built-in Python uuid library](https://docs.python.org/2/library/uuid.html).

* If you do not have a preferred Python testing framework, we recommend [pytest](https://pytest.org/latest/getting-started.html), which makes test-writing and running a lot simpler than the built-in Python `unittest` library. Also, check out the Flask [test client](http://flask.pocoo.org/docs/0.10/testing/#testing) for writing tests against your API routes.

* We've tried to include all of the library imports that you'll need, but if you decide you want to use something else, check out this [documentation for Python imports](https://infohost.nmt.edu/tcc/help/pubs/python/web/import-statement.html).
