""" Sets up the Flask app configuration """

from flask import Flask

app = Flask(__name__)  # pylint: disable=invalid-name

# pylint: disable=wrong-import-position
from household_api.household.views import api

app.register_blueprint(api)

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
