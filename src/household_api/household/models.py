"""
Domain models for validating and marshalling request data
"""

from schematics.models import Model
from schematics.types import IntType, StringType, FloatType, BooleanType
from schematics.types.compound import ModelType, ListType


class User(Model):
    """
    Defines a household member
    """
    age = IntType(required=True)
    gender = StringType(required=True, choices=['male', 'female'])
    pregnant = BooleanType(required=False, serialize_when_none=False)


class Household(Model):
    """
    Defines a household
    """
    members = ListType(ModelType(User), required=True, min_size=1)
    income = FloatType(required=True)
