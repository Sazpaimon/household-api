"""
Endpoints to store and receive household data
"""

import json

import os
import redis
import shortuuid
from flask import request, jsonify, Blueprint, abort
from schematics.exceptions import BaseError
from werkzeug.exceptions import BadRequest, NotFound

from household_api.household.models import Household, User

REDIS_HOST = os.environ.get('REDIS_HOST')
# The baseline threshold, calculated by taking the threshold for a 1
# member household and subtracting the per-person amount
POVERTY_INCOME_BASE = 7880
# The amount, in dollars, that each member of the household adds to the
# poverty level threshold
POVERTY_INCOME_PER_PERSON = 4180
FPL_LIMIT_CHILD = 2.75
FPL_LIMIT_PREGNANT_WOMEN = 2.78
FPL_LIMIT_INFANT = 2.83
FPL_LIMIT_DEFAULT = 2

api = Blueprint('household', __name__)  # pylint: disable=invalid-name


@api.errorhandler(404)
def page_not_found(exc: NotFound):
    """
    Returns a JSON formatted message for 404 errors
    """
    return jsonify(error=404, message=exc.description), 404


@api.errorhandler(400)
def malformed_request(exc: BadRequest):
    """
    Returns a JSON formatted messsage for 400 errors
    """
    return jsonify(error=400, message=exc.description), 400


@api.route('/sample-household', methods=['GET'], strict_slashes=False)
def get_sample_household():
    """ Retrieves a static, sample household object

    This route exists to demonstrate the schema for a Household object.
    It does not touch Redis.
    """
    sample_household = Household({
        'income': 50000,
        'members': [
            {'age': 45, 'gender': 'female'},
            {'age': 40, 'gender': 'male'},
        ],
    })

    return jsonify(sample_household.to_primitive()), 200


def get_redis_connection():
    """
    Returns a connection to the Redis database
    """
    return redis.StrictRedis(host=REDIS_HOST)


def find_household(household_id):
    """
    Finds a household in the Redis DB given an ID. Generates a 404 error
    if a household isn't found

    :param household_id: Household unique ID

    :return: Household class containing the found household data
    """
    redis_conn = get_redis_connection()
    household = redis_conn.get(household_id)
    if not household:
        return abort(404, 'Household not found')
    return Household(json.loads(household))


@api.route('/household/<string:household_id>', methods=['GET'])
def get_household(household_id):
    """
    Endpoint to retrieve data for a given household ID

    Example request:
        GET /household/foo

    Example response:
        {
          "income": 12345.0,
          "members": [
            {
              "age": 1,
              "gender": "male"
            }
          ]
        }

    :param household_id: Household unique ID

    :return: JSON response of household data
    """
    household = find_household(household_id)
    return jsonify(household.to_primitive()), 200


@api.route('/household', methods=['POST'], strict_slashes=False)
def create_household():
    """
    Endpoint which creates a new household record. Returns a 400 if the
    incoming data is malformed

    Example request:
        POST /household

    Example POST body:
        {
          "income": 12345.0,
          "members": [
            {
              "age": 1,
              "gender": "male"
            }
          ]
        }

    Example response (Success):
        {
          "household_id": "7Y7A9v7b8m4ny2j4tbXpZY"
        }

    Example response (Malformed body):
        {
          "error": 400,
          "message": {
            "members": [
              {
                "gender": [
                  "Value must be one of ['male', 'female']."
                ]
              }
            ]
          }
        }

    :return: JSON response of the newly created household ID
    """
    redis_conn = get_redis_connection()
    try:
        household = Household(request.json)
        household.validate()
    except BaseError as exc:
        return abort(400, exc.messages)
    household_id = shortuuid.uuid()
    redis_conn.set(household_id, json.dumps(household.to_primitive()))
    return jsonify({"household_id": household_id}), 200


@api.route('/household/poverty_level/<string:household_id>', methods=['GET'])
def get_household_poverty_level(household_id):
    """
    Endpoint which calculates a household's percentage of Federal Poverty
    Level, given its income and size.

    Example request:
        GET /household/poverty_level/foo

    Example response:
        {
          "income": 12180,
          "percentage_poverty_level": 0.75,
          "poverty_threshold": 16240
        }

    :param household_id: Unique household ID
    :return: JSON response containing the household's income, what its
             poverty threshold is, and its percentage of that threshold
    """
    household = find_household(household_id)
    percentage_poverty_level, poverty_threshold = \
        get_household_fpl_percent(household)
    return jsonify({
        "income": household.income,
        "poverty_threshold": poverty_threshold,
        "percentage_poverty_level": percentage_poverty_level
    }), 200


def get_household_fpl_percent(household: Household):
    """
    Gets the percentage Federal Poverty Limit for a household
    """
    poverty_threshold = POVERTY_INCOME_BASE + (
        POVERTY_INCOME_PER_PERSON * (len(household.members))
    )
    percentage_poverty_level = household.income / poverty_threshold
    return percentage_poverty_level, poverty_threshold


@api.route('/household/medicaid_eligible/<string:household_id>',
           methods=['GET'])
def get_medicaid_eligibility(household_id):
    """
        Endpoint to retrieve household members that are eligible for medicaid

    Example request:
        GET /household/medicaid_eligible/foo

    Example response:
        [
            {"age": 1, "gender": "male"},
            {"age": 25, "gender": "female", "pregnant": True}
        ]

    :param household_id: Household unique ID

    :return: JSON response of household data
    """
    household = find_household(household_id)
    fpl_percent = get_household_fpl_percent(household)
    medicaid_eligible_members = filter(
        lambda member: is_medicaid_eligible(member, fpl_percent),
        household.members
    )
    return jsonify(medicaid_eligible_members), 200


def is_child(age):
    """
    Determines if a particular age qualifies as being a child
    """
    return 2 <= age <= 18


def is_infant(age):
    """
    Determines if a particular age qualifies as being an infant
    """
    return age < 2


def is_medicaid_eligible(household_member: User, household_fpl_percent):
    """
    Determines if a household member is eligible for medicaid based on the
    following conditions

    Children 2-18 eligible @ 275% FPL
    Infants 283% FPL
    Pregnant women 278% FPL
    Everyone 200% FPL
    """
    if household_fpl_percent < FPL_LIMIT_DEFAULT:
        return True
    if is_infant(household_member.age) \
            and household_fpl_percent < FPL_LIMIT_INFANT:
        return True
    if is_child(household_member.age) \
            and household_fpl_percent < FPL_LIMIT_CHILD:
        return True
    if household_member.pregnant \
            and household_fpl_percent < FPL_LIMIT_PREGNANT_WOMEN:
        return True

    return False
